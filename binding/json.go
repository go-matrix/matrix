package binding

import (
	"encoding/json"
	"net/http"
)

// JSON binding
type jsonBinding struct {
}

var (
	JSON = jsonBinding{}
)

func (_ jsonBinding) Bind(req *http.Request, obj interface{}) error {
	decoder := json.NewDecoder(req.Body)
	return decoder.Decode(obj)
}
