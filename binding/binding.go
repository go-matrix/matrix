package binding

import "net/http"

type Binding interface {
	Bind(*http.Request, interface{}) error
}
