package binding

import (
	"encoding/xml"
	"net/http"
)

// XML binding
type xmlBinding struct {
}

var (
	XML = xmlBinding{}
)

func (_ xmlBinding) Bind(req *http.Request, obj interface{}) error {
	decoder := xml.NewDecoder(req.Body)
	return decoder.Decode(obj)
}
