package router

import (
	"fmt"
	"net/http"
	"regexp"
)

type HandlerFunc func(http.ResponseWriter, *http.Request)

type Router struct {
	namedRoutes     map[string]*Route
	routes          []*Route
	hooks           map[string][]http.Handler
	strictSlash     bool
	NotFoundHandler http.Handler
}

func New() *Router {
	return &Router{namedRoutes: make(map[string]*Route)}
}

func (r *Router) getNamedRoutes() map[string]*Route {
	return r.namedRoutes
}

func (r *Router) Get(name string) *Route {
	return r.namedRoutes[name]
}

func (r *Router) GetRoute(name string) *Route {
	return r.namedRoutes[name]
}

func (r *Router) Handle(pattern string, handler http.Handler) *Route {
	route := NewRoute(pattern, handler, r)
	route.StrictSlash(r.strictSlash)
	r.routes = append(r.routes, route)
	return route
}

func (r *Router) HandleFunc(pattern string, f HandlerFunc) *Route {
	return r.Handle(pattern, http.HandlerFunc(f))
}

func (r *Router) StrictSlash(strictSlash bool) *Router {
	r.strictSlash = strictSlash
	return r
}
func (r *Router) GetStrictSlash() bool {
	return r.strictSlash
}

func (r *Router) getMatchedRoutes(httpMethod, resourceUri string) (matchedRoutes []MatchedRoute) {
	for _, route := range r.routes {
		if route.supportsHttpMethod(httpMethod) {
			if matched, ok := route.matches(resourceUri); ok {
				matchedRoutes = append(matchedRoutes, matched)
			}
		}
	}
	return
}

func (r *Router) dispatch(matched MatchedRoute, rw http.ResponseWriter, req *http.Request) bool {
	if rawEncode := matched.params.Encode(); rawEncode != "" {
		req.URL.RawQuery = rawEncode + "&" + req.URL.RawQuery
	}

	matched.handler.ServeHTTP(rw, req)
	return true
}

func (r *Router) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	var dispatched bool
	if matchedRoutes := r.getMatchedRoutes(req.Method, req.URL.Path); len(matchedRoutes) > 0 {
		for _, matched := range matchedRoutes {
			if dispatched = r.dispatch(matched, rw, req); dispatched {
				break
			}
		}
	}

	if !dispatched {
		if r.NotFoundHandler != nil {
			r.NotFoundHandler.ServeHTTP(rw, req)
		} else {
			http.NotFoundHandler().ServeHTTP(rw, req)
		}
	}
}

func (r *Router) UrlFor(name string, params map[string]interface{}) (string, bool) {
	if route, ok := r.namedRoutes[name]; ok {
		url := route.pattern
		for key, value := range params {
			url = regexp.MustCompile(fmt.Sprintf("<[^:/>]*:?%s>", key)).ReplaceAllString(url, fmt.Sprintf("%v", value))
		}
		return url, true
	}
	return "", false
}
