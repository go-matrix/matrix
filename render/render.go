package render

import "net/http"

type Render interface {
	Render(http.ResponseWriter, int, ...interface{}) error
}

func writeHeader(w http.ResponseWriter, code int, contentType string) {
	if code >= 0 {
		w.Header().Set("Content-Type", contentType)
		w.WriteHeader(code)
	}
}
