package render

import (
	"encoding/json"
	"net/http"
)

// JSON binding
type jsonRender struct {
}

var (
	JSON = jsonRender{}
)

func (_ jsonRender) Render(w http.ResponseWriter, code int, data ...interface{}) error {
	writeHeader(w, code, "application/json")
	encoder := json.NewEncoder(w)
	return encoder.Encode(data[0])
}
