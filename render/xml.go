package render

import (
	"encoding/xml"
	"net/http"
)

// xml render
type xmlRender struct {
}

var (
	XML = xmlRender{}
)

func (_ xmlRender) Render(w http.ResponseWriter, code int, data ...interface{}) error {
	writeHeader(w, code, "application/xml")
	encoder := xml.NewEncoder(w)
	return encoder.Encode(data[0])
}
