package render

import (
	"net/http"
	"text/template"
)

// HTML render
type HTMLRender struct {
	Template *template.Template
}

func (html HTMLRender) Render(w http.ResponseWriter, code int, data ...interface{}) error {
	writeHeader(w, code, "text/html")
	file := data[0].(string)
	obj := data[1]
	return html.Template.ExecuteTemplate(w, file, obj)
}
