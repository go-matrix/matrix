package matrix

import (
	"log"
	"os"
	"time"
)

func filterFlags(content string) string {
	for i := 0; i < len(content); i++ {
		if content[i] == ':' {
			return content[:i]
		}
	}
	return content
}

func Logger() HandlerFunc {
	stdlogger := log.New(os.Stdout, "", 0)
	//errlogger := log.New(os.Stderr, "", 0)

	return func(c *Context) {
		// Start timer
		start := time.Now()

		// Process request
		c.Next()

		// save the IP of the remoteAddr
		remoteAddr := c.Request.Header.Get("X-Real-IP")
		// if the remoteAddr is empty, use the hard-coded address from the socket
		if remoteAddr == "" {
			remoteAddr = c.Request.RemoteAddr
		}
		remoteAddr = filterFlags(remoteAddr)

		code := c.Writer.GetStatus()
		end := time.Now()
		latency := end.Sub(start)

		stdlogger.Printf(`%s - - [%v] "%s %s %s" %03d - %v`,
			remoteAddr,
			end.Format("2006/01/02 15:04:05"),
			c.Request.Method, c.Request.Proto, c.Request.URL.Path,
			code,
			latency,
		)
	}
}
