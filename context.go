package matrix

import (
	"errors"
	"net/http"

	"bitbucket.org/go-matrix/matrix/binding"
	"bitbucket.org/go-matrix/matrix/render"
	"bitbucket.org/go-matrix/matrix/router"
	"bitbucket.org/go-matrix/matrix/utils"
)

type Context struct {
	Request  *http.Request
	Writer   ResponseWriter
	Keys     map[string]interface{}
	Engine   *Engine
	handlers []HandlerFunc
	index    int8
}

func (engine *Engine) newContext(w http.ResponseWriter, req *http.Request, handlers []HandlerFunc) *Context {
	c := engine.cache.Get().(*Context)
	c.Writer.Reset(w, c)
	c.Request = req
	c.handlers = handlers
	c.Keys = nil
	c.index = -1
	return c
}

func (c *Context) Copy() *Context {
	var cp Context = *c
	cp.index = AbortIndex
	cp.handlers = nil
	return &cp
}

func (c *Context) Next() {
	c.index++
	s := int8(len(c.handlers))

	for ; c.index < s; c.index++ {
		c.handlers[c.index](c)
	}
}

func (c *Context) Abort(code int) {
	if code >= 0 {
		if !c.Writer.GetWritten() {
			c.Writer.WriteHeader(code)
		}
	}
	c.index = AbortIndex
}

func (c *Context) Set(key string, item interface{}) {
	if c.Keys == nil {
		c.Keys = make(map[string]interface{})
	}
	c.Keys[key] = item
}

func (c *Context) Get(key string) (interface{}, error) {
	if c.Keys != nil {
		if item, ok := c.Keys[key]; ok {
			return item, nil
		}
	}
	return nil, errors.New("key does not exist")
}

func (c *Context) Delete(key string) {
	delete(c.Keys, key)
}

func (c *Context) MustGet(key string) interface{} {
	value, err := c.Get(key)
	if err != nil || value == nil {
		panic("key doesn't exist")
	}
	return value
}

func (c *Context) Bind(obj interface{}) bool {
	var b binding.Binding
	ctype := utils.FilterFlags(c.Request.Header.Get("Content-Type"))

	switch {
	case c.Request.Method == router.HTTPMethodGET || ctype == MIMEPOSTForm:
		b = binding.Form
	case ctype == MIMEJSON:
		b = binding.JSON
	case ctype == MIMEXML || ctype == MIMEXML2:
		b = binding.XML
	default:
		c.Abort(400)
		return false
	}
	return c.BindWith(obj, b)
}

func (c *Context) BindWith(obj interface{}, b binding.Binding) bool {
	if err := b.Bind(c.Request, obj); err != nil {
		c.Abort(400)
		return false
	}
	return true
}

func (c *Context) Render(code int, render render.Render, obj ...interface{}) {
	if err := render.Render(c.Writer, code, obj...); err != nil {
		c.Abort(500)
	}
}

func (c *Context) JSON(code int, obj interface{}) {
	c.Render(code, render.JSON, obj)
}

func (c *Context) HTML(code int, name string, obj interface{}) {
	c.Render(code, c.Engine.HTMLRender, name, obj)
}

func (c *Context) String(code int, format string, values ...interface{}) {
	c.Render(code, render.Plain, format, values)
}

func (c *Context) Data(code int, contentType string, data []byte) {
	if len(contentType) > 0 {
		c.Writer.Header().Set("Content-Type", contentType)
	}

	if code >= 0 {
		c.Writer.WriteHeader(code)
	}
	c.Writer.Write(data)
}

func (c *Context) File(filepath string) {
	http.ServeFile(c.Writer, c.Request, filepath)
}
