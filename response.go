package matrix

import "net/http"

type ResponseWriter interface {
	http.ResponseWriter
	GetStatus() int
	GetWritten() bool
	Status(int)

	Reset(http.ResponseWriter, *Context)
}

type ServerWriter struct {
	c *Context
	http.ResponseWriter
	status  int
	written bool
	fired   bool
}

func (w *ServerWriter) Status(code int) {
	w.status = code
}

func (w *ServerWriter) Reset(writer http.ResponseWriter, c *Context) {
	w.c = c
	w.status = 0
	w.written = false
	w.fired = false
	w.ResponseWriter = writer
}

func (w *ServerWriter) WriteHeader(code int) {
	w.status = code
	w.written = true
	w.ResponseWriter.WriteHeader(code)
	w.fireStatusHandler(code)
}

func (w *ServerWriter) Write(data []byte) (int, error) {
	if !w.fired {
		return w.ResponseWriter.Write(data)
	}
	return 0, nil
}

func (w *ServerWriter) fireStatusHandler(code int) {
	if handlers := w.c.Engine.GetStatusHandler(code); handlers != nil {
		for _, handler := range handlers {
			handler(w.c)
			w.fired = true
		}
	}
}

func (w *ServerWriter) GetStatus() int {
	return w.status
}

func (w *ServerWriter) GetWritten() bool {
	return w.written
}
