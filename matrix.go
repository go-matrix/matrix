package matrix

import (
	"net/http"
	"path"
	"sync"
	"text/template"

	"bitbucket.org/go-matrix/matrix/render"
	"bitbucket.org/go-matrix/matrix/router"
)

type RouterGroup struct {
	Handlers []HandlerFunc
	prefix   string
	parent   *RouterGroup
	engine   *Engine
}

type Engine struct {
	*RouterGroup
	cache          sync.Pool
	HTMLRender     render.Render
	router         *router.Router
	statusHandlers map[int][]HandlerFunc
	errorHandlers  map[error][]HandlerFunc
	noRoute        []HandlerFunc
	finalNoRoute   []HandlerFunc
}

func (engine *Engine) handle404(w http.ResponseWriter, req *http.Request) {
	c := engine.newContext(w, req, engine.finalNoRoute)
	c.Writer.Status(http.StatusNotFound)
	//c.Writer.WriteHeader(http.StatusNotFound)
	c.Next()
	if !c.Writer.GetWritten() {
		c.Data(http.StatusNotFound, MIMEPlain, []byte("404 page not found"))
	}
	engine.cache.Put(c)
}

func New() *Engine {
	engine := &Engine{}
	engine.RouterGroup = &RouterGroup{nil, "/", nil, engine}
	engine.router = router.New()
	engine.errorHandlers = make(map[error][]HandlerFunc)
	engine.statusHandlers = make(map[int][]HandlerFunc)
	engine.router.NotFoundHandler = http.HandlerFunc(engine.handle404)
	engine.cache.New = func() interface{} {
		c := &Context{Engine: engine}
		c.Writer = &ServerWriter{}
		return c
	}
	return engine
}

func Default() *Engine {
	engine := New()
	engine.Use(Recovery(), Logger())
	return engine
}

func (engine *Engine) StrictSlash(strictSlash bool) *Engine {
	engine.router.StrictSlash(strictSlash)
	return engine
}

func (engine *Engine) StatusHandler(code int, handlers ...HandlerFunc) {
	engine.statusHandlers[code] = append(engine.statusHandlers[code], handlers...)
}

func (engine *Engine) GetStatusHandler(code int) []HandlerFunc {
	return engine.statusHandlers[code]
}

func (engine *Engine) ErrorHandler(err error, handlers ...HandlerFunc) {
	engine.errorHandlers[err] = append(engine.errorHandlers[err], handlers...)
}

func (engine *Engine) GetErrorHandler(err error) []HandlerFunc {
	return engine.errorHandlers[err]
}

func (engine *Engine) LoadHTMLGlob(pattern string) {
	templ := template.Must(template.ParseGlob(pattern))
	engine.SetHTMLTemplate(templ)
}

func (engine *Engine) LoadHTMLFiles(files ...string) {
	templ := template.Must(template.ParseFiles(files...))
	engine.SetHTMLTemplate(templ)
}

func (engine *Engine) SetHTMLTemplate(templ *template.Template) {
	engine.HTMLRender = render.HTMLRender{
		Template: templ,
	}
}

func (engine *Engine) NoRoute(handlers ...HandlerFunc) {
	engine.noRoute = handlers
	engine.finalNoRoute = engine.combineHandlers(engine.noRoute)
}

func (engine *Engine) Use(middlewares ...HandlerFunc) {
	engine.RouterGroup.Use(middlewares...)
	engine.finalNoRoute = engine.combineHandlers(engine.noRoute)
}

func (engine *Engine) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	engine.router.ServeHTTP(w, req)
}

func (engine *Engine) Run(addr string) {
	if err := http.ListenAndServe(addr, engine); err != nil {
		panic(err)
	}
}

func (engine *Engine) RunTLS(addr string, cert string, key string) {
	if err := http.ListenAndServeTLS(addr, cert, key, engine); err != nil {
		panic(err)
	}
}

func (group *RouterGroup) Use(middlewares ...HandlerFunc) {
	group.Handlers = append(group.Handlers, middlewares...)
}

func (group *RouterGroup) pathFor(p string) string {
	joined := path.Join(group.prefix, p)
	// Append a '/' if the last component had one, but only if it's not there already
	if len(p) > 0 && p[len(p)-1] == '/' && joined[len(joined)-1] != '/' {
		return joined + "/"
	}
	return joined
}

func (group *RouterGroup) Group(component string, handlers ...HandlerFunc) *RouterGroup {
	prefix := group.pathFor(component)

	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		parent:   group,
		prefix:   prefix,
		engine:   group.engine,
	}
}

func (group *RouterGroup) HandleFunc(pattern string, handlers []HandlerFunc) *router.Route {
	pattern = group.pathFor(pattern)
	handlers = group.combineHandlers(handlers)
	return group.engine.router.HandleFunc(pattern, func(w http.ResponseWriter, req *http.Request) {
		c := group.engine.newContext(w, req, handlers)
		c.Next()
		group.engine.cache.Put(c)
	})
}

func (group *RouterGroup) GET(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("GET")
}

func (group *RouterGroup) POST(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("POST")
}

func (group *RouterGroup) PUT(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("PUT")
}

func (group *RouterGroup) DELETE(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("DELETE")
}

func (group *RouterGroup) PATCH(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("PATCH")
}

func (group *RouterGroup) OPTIONS(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("OPTIONS")
}

func (group *RouterGroup) HEAD(pattern string, handlers ...HandlerFunc) *router.Route {
	return group.HandleFunc(pattern, handlers).Methods("HEAD")
}

func (group *RouterGroup) Static(p, root string) {
	prefix := group.pathFor(p)
	p = path.Join(p, "/<path:filepath>")
	fileServer := http.StripPrefix(prefix, http.FileServer(http.Dir(root)))
	group.GET(p, func(c *Context) {
		fileServer.ServeHTTP(c.Writer, c.Request)
	})
	group.HEAD(p, func(c *Context) {
		fileServer.ServeHTTP(c.Writer, c.Request)
	})
}

func (group *RouterGroup) combineHandlers(handlers []HandlerFunc) []HandlerFunc {
	s := len(group.Handlers) + len(handlers)
	h := make([]HandlerFunc, 0, s)
	h = append(h, group.Handlers...)
	h = append(h, handlers...)
	return h
}
