package matrix

import "math"

const (
	AbortIndex = math.MaxInt8 / 2

	MIMEJSON     = "application/json"
	MIMEXML      = "application/xml"
	MIMEXML2     = "text/xml"
	MIMEHTML     = "text/html"
	MIMEPlain    = "text/plain"
	MIMEPOSTForm = "application/x-www-form-urlencoded"
)

type HandlerFunc func(*Context)
