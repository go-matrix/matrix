package main

import "bitbucket.org/go-matrix/matrix"

func main() {
	r := matrix.Default()
	r.GET("/hello/", func(c *matrix.Context) {
		c.String(200, "hello world!")
	}).StrictSlash(true)

	r.GET("/v/<path:name>/", func(c *matrix.Context) {
		name := c.Request.URL.Query().Get("name")
		c.String(200, "%s", name)
	})

	r.Static("/", "/tmp")

	r.StatusHandler(200, func(c *matrix.Context) {
		c.Writer.Write([]byte("200 error"))
	})

	r.StatusHandler(404, func(c *matrix.Context) {
		c.Writer.Write([]byte("404 not found"))
	})

	r.NoRoute(func(c *matrix.Context) {
	})

	r.Run(":8080")
}
